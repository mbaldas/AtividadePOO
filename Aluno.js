"use strict";
exports.__esModule = true;
var Aluno = /** @class */ (function () {
    function Aluno() {
    }
    Aluno.prototype.getNota = function () {
        return this.nota;
    };
    Aluno.prototype.getCpf = function () {
        return this.cpf;
    };
    Aluno.prototype.getNome = function () {
        return this.nome;
    };
    Aluno.prototype.getMatricula = function () {
        return this.matricula;
    };
    Aluno.prototype.setNota = function (nota) {
        this.nota = nota;
    };
    Aluno.prototype.setCpf = function (cpf) {
        this.cpf = cpf;
    };
    Aluno.prototype.setNome = function (nome) {
        this.nome = nome;
    };
    Aluno.prototype.setMatricula = function (matricula) {
        this.matricula = matricula;
    };
    return Aluno;
}());
exports.Aluno = Aluno;
var aluno1 = new Aluno();
aluno1.setCpf("10574422658");
aluno1.setNome("Matheus");
aluno1.setMatricula(12);
