"use strict";
exports.__esModule = true;
var Aluno_1 = require("./Aluno");
var Professor = /** @class */ (function () {
    function Professor(nome, salario) {
        this.nome = nome;
        this.salario = salario;
    }
    Professor.prototype.atribuiNota = function (a, nota) {
        a.setNota(nota);
    };
    return Professor;
}());
exports.Professor = Professor;
var aluno = new Aluno_1.Aluno();
var p = new Professor("Pedro", 2500);
p.atribuiNota(aluno, 10);
