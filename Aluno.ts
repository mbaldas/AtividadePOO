export class Aluno{
    private matricula: number;
    private nota: number;
    private cpf: string;
    private nome: string;

    public getNota(): number {
        return this.nota;
    }

    public getCpf(): string{
        return this.cpf;
    }

    public getNome(): string{
        return this.nome;
    }

    public getMatricula(): number{
        return this.matricula;
    }

    public setNota(nota:number){
        this.nota = nota;
    }

    public setCpf(cpf:string) {
        this.cpf = cpf;
    
    }
    public setNome(nome:string){
        this.nome = nome;
    }

    public setMatricula(matricula:number){
        this.matricula = matricula;
    }

}

let aluno1: Aluno = new Aluno ();

aluno1.setCpf ("10574422658");
aluno1.setNome("Matheus");
aluno1.setMatricula(12);


