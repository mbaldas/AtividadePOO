import { Aluno } from './Aluno';

export class Professor{
    private nome: string;
    public salario: number;

    public  atribuiNota(a: Aluno, nota: number){
        a.setNota(nota);
    }

    constructor (nome: string, salario: number){
        this.nome = nome;
        this.salario = salario;
    }

}

let aluno: Aluno = new Aluno();

let p: Professor = new Professor("Pedro", 2500);

p.atribuiNota(aluno, 10);